function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    
    if (arguments.length < 2 || typeof cb !== 'function' || typeof n !== 'number') {
        throw new Error("Error");
    }


    function funtionCall(...args) {
        if (n < 1) {
            return null;
        }
        n--;
        return cb(...args);
    }

    return funtionCall;
}

function cb() {
    return "Hello";
}


module.exports = {limitFunctionCallCount,cb};
