const {cacheFunction,cb} = require("../cacheFunction.cjs");

test("Testing cache function",() => {
    let fun = cacheFunction(cb);
    expect(fun(1,2,3)).toBe(6);
    expect(fun(1,3,4)).toBe(8);
    expect(fun(1,2,3)).toBe(6);
})