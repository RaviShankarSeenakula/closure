const {limitFunctionCallCount,cb} = require("../limitFunctionCallCount.cjs");
// const cb = require("../limitFunctionCallCount.cjs");

test("Testing limit function call count",() => {
    let fun = limitFunctionCallCount(cb,2);
    expect(fun()).toBe("Hello");
    expect(fun()).toBe("Hello");
    expect(fun()).toBe(null);
})