function cacheFunction(cb) {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
    if (typeof cb !== 'function' || arguments.length < 1) {
        throw new Error("Error");
    }

    const cache = {};

    function funtionCall (...args) {
        // console.log(args);
        // console.log(cache);
        if (args in cache) {
            return cache[args];
        } else {
            cache[args] = cb(...args);
            return cache[args];
        }
    }

    return funtionCall;
}


function cb(a,b,c) {
    return a+b+c;
}

module.exports = {cacheFunction, cb}; 